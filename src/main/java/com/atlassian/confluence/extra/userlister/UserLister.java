package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.extra.userlister.model.UserList;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.descriptor.web.conditions.PeopleDirectoryEnabledCondition;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.Group;
import com.atlassian.user.search.page.Pager;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class UserLister extends BaseMacro implements Macro
{
    private static final Logger logger = LoggerFactory.getLogger(UserLister.class);

    private final UserAccessor userAccessor;

    private final UserListManager userListManager;

    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    private final VelocityHelperService velocityHelperService;

    private final SettingsManager settingsManager;

    private final PermissionManager permissionManager;

    public UserLister(UserAccessor userAccessor, UserListManager userListManager, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, VelocityHelperService velocityHelperService, SettingsManager settingsManager, PermissionManager permissionManager)
    {
        this.userAccessor = userAccessor;
        this.userListManager = userListManager;
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
        this.velocityHelperService = velocityHelperService;
        this.settingsManager = settingsManager;
        this.permissionManager = permissionManager;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    private String getText(String key)
    {
        return getI18NBean().getText(key);
    }

    private String getText(String key, List params)
    {
        return getI18NBean().getText(key, params);
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        try 
        {
			return execute(parameters, body, new DefaultConversionContext(renderContext));
		} 
        catch (MacroExecutionException e) 
        {
			throw new MacroException(e);
		}
    }

    private String createCSVList(List<String> emptyGroups)
    {
        return getText("userlister.noresultsfoundforgroups", Arrays.asList(StringUtils.join(emptyGroups, ',')));
    }

    private Set<String> getGroups(final String groupNames)
    {
        String[] groupNameArray = StringUtils.split(groupNames, ',');
        Set<String> groups = new LinkedHashSet<String>();

        for (String groupName : groupNameArray)
            if (StringUtils.isNotBlank(groupName))
                groups.add(StringUtils.trim(groupName));

        /* We need to check if the user specified * in the group(s) macro parameter.
         * If he/she did, we need to expand it to all groups in Confluence
         */
        if (groups.contains(UserList.ALL_GROUP_NAME))
        {
            Pager<Group> filteredExpandedGroups = userAccessor.filterUnaccessibleGroups(userAccessor.getGroups(), AuthenticatedUserThreadLocal.getUser());
            for (Group group : filteredExpandedGroups)
                groups.add(group.getName());

            /* Then remove the wildcard */
            groups.remove(UserList.ALL_GROUP_NAME);
        }

        return groups;
    }

    private Set<String> getAllowedGroups(Set<String> groups)
    {
        Set<String> allowedGroups = new TreeSet<String>(groups);
        allowedGroups.removeAll(userListManager.getGroupBlackList());

        return allowedGroups;
    }

    private Set<String> getDeniedGroups(Set<String> groups)
    {
        Set<String> deniedGroups = new TreeSet<String>(groups);
        deniedGroups.retainAll(userListManager.getGroupBlackList());
        
        return deniedGroups;
    }

	public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException 
	{
        if (isPeopleDirectoryDisabled())
            return RenderUtils.blockError(
                    getText("userlister.notpermitted.viewuserprofile"),
                    ""
            );


		//1. parse parameters from macro
        String groupNames = StringUtils.defaultString(parameters.get("groups"), parameters.get("group"));
        boolean returnOnlineUsers = Boolean.parseBoolean(StringUtils.trim(parameters.get("online")));
        Boolean showWarning = Boolean.valueOf(StringUtils.defaultString(StringUtils.trim( parameters.get("showWarning")), "true"));

        Set blackListedGroups = userListManager.getGroupBlackList();
        
        if(StringUtils.isBlank(groupNames))
            return getText("userlister.no.groups.specified");

        if (ArrayUtils.contains(StringUtils.split(groupNames, ','), UserList.ALL_GROUP_NAME)
                && blackListedGroups.contains(UserList.ALL_GROUP_NAME))
            return getText("userlister.group.name.list.contains.asterisk");

        Set<String> groups = getGroups(groupNames);
        Set<String> allowedGroups = getAllowedGroups(groups);
        Set<String> deniedGroups = getDeniedGroups(groups);

        Collection<String> loggedInUsernames = userListManager.getLoggedInUsers();
        
        ArrayList<UserList> groupList = new ArrayList<UserList>();
        List<String> emptyGroups = new ArrayList<String>();

        UserList.ListMode listMode = !parameters.containsKey("online")
                ? UserList.ListMode.all
                : (returnOnlineUsers ? UserList.ListMode.online_only : UserList.ListMode.offline_only);

        for (String currentGroup : allowedGroups)
        {
            UserList userList = new UserList(currentGroup, userAccessor);

            if (userList.isUseSpecificGroupName() || blackListedGroups.isEmpty())
            {
                userList.build(loggedInUsernames, listMode);
                if (!userList.getUsers().isEmpty())
                    groupList.add(userList);
                else
                    emptyGroups.add(currentGroup);
            }
        }

        // now create a simple velocity context and render a template for the output
        Map<String, Object> contextMap = velocityHelperService.createDefaultVelocityContext();

        if (!deniedGroups.isEmpty())
            contextMap.put("deniedGroups", deniedGroups);

        contextMap.put("showWarning", showWarning);
        contextMap.put("userlists", groupList);
        if (listMode != UserList.ListMode.all)
            contextMap.put("online", returnOnlineUsers);
        else
            contextMap.put("allUserStatuses", true);
        
        if(emptyGroups.size() > 0)
        {
            contextMap.put("emptyGroups", createCSVList(emptyGroups));
        }

        try
        {
            return velocityHelperService.getRenderedTemplate("templates/extra/userlister/userlistermacro.vm", contextMap);
        }
        catch (Exception e)
        {
            logger.error("Error while trying to display UserList!", e);
            return getText("userlister.unable.to.render.result", Arrays.asList(e.toString()));
        }
	}

    private boolean isPeopleDirectoryDisabled()
    {
        PeopleDirectoryEnabledCondition peopleDirectoryEnabledCondition = new PeopleDirectoryEnabledCondition();
        peopleDirectoryEnabledCondition.setPermissionManager(permissionManager);
        return peopleDirectoryEnabledCondition.isPeopleDirectoryDisabled(AuthenticatedUserThreadLocal.getUser());
    }

	public BodyType getBodyType() 
	{
		return BodyType.NONE;
	}

	public OutputType getOutputType() 
	{
		return OutputType.BLOCK;
	}
}

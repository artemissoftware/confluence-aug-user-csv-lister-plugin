package com.atlassian.confluence.extra.userlister.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.userlister.UserListManager;
import com.atlassian.confluence.extra.userlister.model.UserList;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import static com.google.common.base.Joiner.on;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Collections2.transform;

public class ConfigureUserListsAction extends ConfluenceActionSupport
{
    private static final Logger logger = LoggerFactory.getLogger(ConfigureUserListsAction.class);

    private String blackListEntries;

    private UserListManager userListManager;

    private UserAccessor userAccessor;

    private String save;

    public Set<String> getBlackListEntiresAsList() throws IOException
    {
        BufferedReader bufferedReader = null;

        try
        {
            String line;
            bufferedReader = new BufferedReader(new StringReader(StringUtils.defaultString(blackListEntries)));
            final Set<String> blackListEntries = new TreeSet<String>();

            while (null != (line = bufferedReader.readLine()))
                blackListEntries.add(StringUtils.trim(line));

            return blackListEntries;
        }
        finally
        {
            IOUtils.closeQuietly(bufferedReader);
        }
    }

    public String getBlackListEntries()
    {
        return blackListEntries;
    }

    public void setBlackListEntries(String blackListEntries)
    {
        this.blackListEntries = blackListEntries;
    }

    public void setUserListManager(UserListManager userListManager)
    {
        this.userListManager = userListManager;
    }

    @Override
    public void setUserAccessor(UserAccessor userAccessor)
    {
        super.setUserAccessor(userAccessor);
        this.userAccessor = userAccessor;
    }

    public void setSave(String save)
    {
        this.save = save;
    }

    @Override
    public String doDefault() throws Exception
    {
        setBlackListEntries(StringUtils.join(userListManager.getGroupBlackList(), "\n"));
        return SUCCESS;
    }

    @Override
    public String execute() throws Exception
    {
        if (StringUtils.equals(getText("add.name"), save))
        {
            userListManager.saveGroupBlackList(getBlackListEntiresAsList());

            addActionMessage(getText("userlister.configure.successful"));
            return SUCCESS;
        }

        return CANCEL;
    }

    @Override
    public void validate()
    {

        final Predicate<String> groupPredicate = new Predicate<String>()
        {
            public boolean apply(String groupName)
            {
                return !StringUtils.equals(UserList.ALL_GROUP_NAME, groupName) && null == userAccessor.getGroup(groupName);
            }
        };

        //TODO: move this function to GeneralUtil class
        final Function<String, String> groupTransformer = new Function<String, String>()
        {
            @Override
            public String apply(@Nullable String groupName)
            {
                return GeneralUtil.htmlEncode(groupName);
            }
        };

        try
        {
            final Collection<String> invalidGroupNames = filter(getBlackListEntiresAsList(), groupPredicate);
            final Collection<String> encodedInvalidGroupNames = transform(invalidGroupNames, groupTransformer);
            final String joinedEncodedInvalidGroupNames = on(", ").join(encodedInvalidGroupNames);

            if (!invalidGroupNames.isEmpty())
                addActionError(getText("userlister.configure.invalid.group.names"), joinedEncodedInvalidGroupNames);
        }
        catch (final IOException ioe)
        {
            logger.error("Unable to perform action validation", ioe);
            addActionError(getText("userlister.configure.ioexception"));
        }
    }


}

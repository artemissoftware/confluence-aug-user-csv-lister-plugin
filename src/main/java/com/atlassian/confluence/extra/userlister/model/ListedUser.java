package com.atlassian.confluence.extra.userlister.model;

import com.atlassian.user.User;

public class ListedUser
{
    private User baseUser;
    private boolean loggedIn;

    public ListedUser(User user, boolean loggedIn)
    {
        this.baseUser = user;
        this.loggedIn = loggedIn;
    }

    public String getFullName()
    {
        return baseUser.getFullName();
    }

    public String getName()
    {
        return baseUser.getName();
    }

    public String getEmail()
    {
        return baseUser.getEmail();
    }

    public boolean isLoggedIn()
    {
        return loggedIn;
    }

    public boolean getLoggedIn()
    {
        return loggedIn;
    }
}

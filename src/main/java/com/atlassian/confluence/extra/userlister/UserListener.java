package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;

public class UserListener implements EventListener
{
    private final UserListManager userListManager;

    public UserListener(UserListManager userListManager)
    {
        this.userListManager = userListManager;
    }

    private static final Class[] HANDLED_CLASSES = new Class[]{ LoginEvent.class, LogoutEvent.class};

    public void handleEvent(Event event)
    {
        if (event instanceof LoginEvent)
        {
            LoginEvent loginEvent = (LoginEvent) event;
            userListManager.registerLoggedInUser(loginEvent.getUsername(), loginEvent.getSessionId());
        }
        else if (event instanceof LogoutEvent)
        {
            LogoutEvent logoutEvent = (LogoutEvent) event;
            userListManager.unregisterLoggedInUser(logoutEvent.getUsername(), logoutEvent.getSessionId());
        }
    }

    public Class[] getHandledEventClasses()
    {
        return HANDLED_CLASSES;
    }
}

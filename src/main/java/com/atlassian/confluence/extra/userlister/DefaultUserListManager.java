package com.atlassian.confluence.extra.userlister;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DefaultUserListManager implements UserListManager
{
    private final BandanaManager bandanaManager;

    private final CacheManager cacheManager;

    public DefaultUserListManager(BandanaManager bandanaManager, CacheManager cacheManager)
    {
        this.bandanaManager = bandanaManager;
        this.cacheManager = cacheManager;
    }

    public Set<String> getGroupBlackList()
    {
        @SuppressWarnings("unchecked")
        final Set<String> blackList = (Set<String>) bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_BLACK_LIST);
        return null == blackList ? Collections.<String> emptySet() : blackList;
    }

    public void saveGroupBlackList(Set<String> deniedGroups)
    {
        bandanaManager.setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, BANDANA_KEY_BLACK_LIST, deniedGroups);
    }

    public boolean isGroupPermitted(String groupName)
    {
        return !StringUtils.isNotBlank(groupName) || !getGroupBlackList().contains(groupName);
    }

    private Cache getLoggedInUsersCache()
    {
        return cacheManager.getCache(getClass().getName());
    }

    @SuppressWarnings("unchecked")
    public Set<String> getLoggedInUsers()
    {
        return new HashSet<String>(getLoggedInUsersCache().getKeys());
    }

    private Set<String> getLoggedInUserSessionIds(String userName)
    {
        @SuppressWarnings("unchecked")
        Set<String> sessionIds = (Set<String>) getLoggedInUsersCache().get(userName);
        return null == sessionIds
                ? new HashSet<String>()
                : sessionIds;
    }

    private void cacheLoggedInUser(String userName, Set<String> sessionIds)
    {
        getLoggedInUsersCache().put(userName, sessionIds);
    }

    public void registerLoggedInUser(String userName, String sessionId)
    {
        Set<String> sessionIds = getLoggedInUserSessionIds(userName);

        sessionIds.add(sessionId);
        cacheLoggedInUser(userName, sessionIds);
    }

    public void unregisterLoggedInUser(String userName, String sessionId)
    {
        Set<String> sessionIds = getLoggedInUserSessionIds(userName);

        sessionIds.remove(sessionId);
        if (sessionIds.isEmpty())
        {
            getLoggedInUsersCache().remove(userName);
        }
        else
        {
            cacheLoggedInUser(userName, sessionIds);
        }
    }
}

package com.atlassian.confluence.extra.userlister.model;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import com.atlassian.user.search.page.Pager;
import com.atlassian.user.search.page.PagerUtils;

import java.util.*;

/**
 * User: nickf
 * Date: Sep 8, 2004
 */
public class UserList
{
    private String groupName;
    private List<ListedUser> users;
    private UserAccessor userAccessor;
    private boolean useSpecificGroupName;
    
    public static final String ALL_GROUP_NAME = "*";

    public static enum ListMode
    {
        all,
        online_only,
        offline_only
    }


    public UserList(String groupName, UserAccessor userAccessor)
    {
        this.groupName = groupName;
        this.userAccessor = userAccessor;
        useSpecificGroupName = !groupName.equals(ALL_GROUP_NAME);
    }

    public String getGroup()
    {
        return groupName;
    }

    public List<ListedUser> getUsers()
    {
        return users;
    }

    public void build(Collection loggedInUsers, ListMode listMode)
    {
        List usernames = getUsersByGroup();

        users = new ArrayList<ListedUser>(usernames.size());

        for (Object next: usernames)
        {
            User user = next instanceof User
                    ? (User) next
                    : userAccessor.getUser((String) next);

            // If it's an anonymous user, then we can't do anything (CONF-5821)
            if (user == null)
                continue;

            // we don't know group users are active, so check.
            if (useSpecificGroupName && userAccessor.isDeactivated(user))
                continue;
            
            boolean isLoggedIn = loggedInUsers.contains(user.getName());
            if (listMode == ListMode.all)
            {
                users.add(new ListedUser(user, isLoggedIn));
            }
            else
            {
                if (listMode == ListMode.online_only && isLoggedIn)
                    users.add(new ListedUser(user, isLoggedIn));
                if (listMode == ListMode.offline_only && !isLoggedIn)
                    users.add(new ListedUser(user, isLoggedIn));
            }
        }

        sortUsers();
    }

    public boolean isUseSpecificGroupName()
    {
        return useSpecificGroupName;
    }

    private List getUsersByGroup()
    {
        Pager usernames = null;

        if (!useSpecificGroupName)
        {
            usernames = userAccessor.getUsersWithConfluenceAccess();
        }
        else //go hunting for the specific group
        {
            Group group = userAccessor.getGroup(groupName);

            if (group != null)
                usernames = userAccessor.getMemberNames(group);
        }

        return usernames == null ? Collections.EMPTY_LIST : PagerUtils.toList(usernames);
    }

    private void sortUsers()
    {
        Collections.sort(users, new Comparator()
        {
            public int compare(Object o1, Object o2)
            {
                ListedUser u1 = (ListedUser) o1;
                ListedUser u2 = (ListedUser) o2;
                if (u1 == null && u2 == null)
                    return 0;
                if (u2 == null)
                    return -1;
                if (u1 == null)
                    return 1;
                String name1 = u1.getFullName();
                String name2 = u2.getFullName();
                if (name1 == null)
                    name1 = u1.getName();
                if (name2 == null)
                    name2 = u2.getName();
                if (name1 == null || name2 == null)
                    throw new RuntimeException("Null user name");
                else
                    return name1.toLowerCase().compareTo(name2.toLowerCase());
            }
        });
    }
}

package com.atlassian.confluence.extra.userlister.model;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultGroup;
import com.atlassian.user.impl.DefaultUser;
import com.atlassian.user.search.page.DefaultPager;
import junit.framework.TestCase;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.when;

public class UserListTestCase extends TestCase
{

    private static final String ADMIN_USER_NAME = "admin";

    private static final String JDOE_USER_NAME = "jdoe";
    
    private static final String CONFLUENCE_USERS_GROUP = "confluence-users";

    @Mock
    private UserAccessor userAccessor;
    
    private UserList userList;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(userAccessor.getUser(anyString())).thenAnswer(
                new Answer<User>()
                {
                    public User answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return new DefaultUser(invocationOnMock.getArguments()[0].toString());
                    }
                }
        );
        when(userAccessor.getGroup(CONFLUENCE_USERS_GROUP)).thenReturn(new DefaultGroup(CONFLUENCE_USERS_GROUP));
        when(
                userAccessor.getMemberNames(
                        argThat(
                                new ArgumentMatcher<Group>()
                                {
                                    public boolean matches(Object o)
                                    {
                                        return ((Group) o).getName().equals(CONFLUENCE_USERS_GROUP);
                                    }
                                }
                        )
                )
        ).thenReturn(new DefaultPager(Arrays.asList(ADMIN_USER_NAME, JDOE_USER_NAME)));

        userList = new UserList(CONFLUENCE_USERS_GROUP, userAccessor);
    }

    public void testAllUsersListedIfListModeIsAll()
    {
        userList.build(Arrays.asList(ADMIN_USER_NAME, JDOE_USER_NAME), UserList.ListMode.all);
        List<ListedUser> users = userList.getUsers();

        assertEquals(2, users.size());
        assertEquals(ADMIN_USER_NAME, users.get(0).getName());
        assertEquals(JDOE_USER_NAME, users.get(1).getName());
    }

    public void testOnlineUsersListedOnlyIfListModeIsOnline()
    {
        userList.build(Arrays.asList(ADMIN_USER_NAME), UserList.ListMode.online_only);
        List<ListedUser> users = userList.getUsers();

        assertEquals(1, users.size());
        assertEquals(ADMIN_USER_NAME, users.get(0).getName());
    }

    public void testOfflineUsersListedOnlyIfListModeIsOffline()
    {
        userList.build(Arrays.asList(JDOE_USER_NAME), UserList.ListMode.offline_only);
        List<ListedUser> users = userList.getUsers();

        assertEquals(1, users.size());
        assertEquals(ADMIN_USER_NAME, users.get(0).getName());
    }
    
}

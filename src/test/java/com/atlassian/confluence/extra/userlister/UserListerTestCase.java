package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.extra.userlister.model.UserList;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultGroup;
import com.atlassian.user.impl.DefaultUser;
import com.atlassian.user.search.page.DefaultPager;
import com.atlassian.user.search.page.Pager;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserListerTestCase extends TestCase
{
    @Mock
    private UserAccessor userAccessor;

    @Mock
    private UserListManager userListManager;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
    private VelocityHelperService velocityHelperService;

    @Mock
    private SettingsManager settingsManager;

    @Mock
    private PermissionManager permissionManager;

    private UserLister userLister;

    private Map<String, String> macroParameters;

    private Page pageToRender;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return (String) invocationOnMock.getArguments()[0];
                    }
                }
        );
        when(i18NBean.getText(anyString(), (List) anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return (String) invocationOnMock.getArguments()[0];
                    }
                }
        );
        
        userLister = new UserLister(userAccessor, userListManager, localeManager, i18NBeanFactory, velocityHelperService, settingsManager, permissionManager);
//        when(spacePermissionManager.hasPermission(eq(SpacePermission.VIEW_USER_PROFILES_PERMISSION), eq((Space) null), Matchers.<User>anyObject())).thenReturn(true);

        when(permissionManager.hasPermission(
                Matchers.<User>anyObject(),
                eq(Permission.VIEW),
                eq(PermissionManager.TARGET_PEOPLE_DIRECTORY)
        )).thenReturn(true);

        macroParameters = new HashMap<String, String>();
        pageToRender = new Page();
    }

    public void testErrorMessageRenderedIfGroupNotSpecifiedInMacroParameters() throws MacroException
    {
        when(userListManager.getGroupBlackList()).thenReturn(Collections.<String> emptySet());
        
        assertEquals(
                "userlister.no.groups.specified",
                userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext())
        );
    }

    public void testErrorMessageRenderedIfWildcardSpecifiedAndBlacklistedGroupContainAsteriskAsOneEntry() throws MacroException
    {

        when(userListManager.getGroupBlackList()).thenReturn(new HashSet<String>(Arrays.asList( UserList.ALL_GROUP_NAME )));

        macroParameters.put("groups", UserList.ALL_GROUP_NAME);

        assertEquals(
                "userlister.group.name.list.contains.asterisk",
                userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext())
        );
    }

    public void testGroupsProperlyGroupedIntoDeniedAllowedAndEmptyCategories() throws MacroException
    {
        final Collection<String> blacklistedGroups = Arrays.asList( "confluence-users" );
        final String confluenceAdminGroupName = "confluence-administrators";
        final Group confluenceAdminGroup = new DefaultGroup(confluenceAdminGroupName);
        final User confluenceAdminMember = new DefaultUser("admin");
        final String newGroupName = "new-group";
        final Group newGroup = new DefaultGroup(newGroupName);
        final String inactiveUsersOnlyGroupName = "inactive-group";
        final Group inactiveUsersGroup = new DefaultGroup(inactiveUsersOnlyGroupName);
        final User inactiveMember = new DefaultUser("inactive-member");

        when(userListManager.getGroupBlackList()).thenReturn(new HashSet<String>(blacklistedGroups));

        
        /* Mock scenario where "admin" in group "confluence-administrators" is valid and active. */
        when(userAccessor.getGroup(confluenceAdminGroupName)).thenReturn(confluenceAdminGroup);
        when(userAccessor.getMemberNames(confluenceAdminGroup)).thenReturn(
                new DefaultPager<String>(Arrays.asList( confluenceAdminMember.getName() ))
        );
        when(userAccessor.getUser(confluenceAdminMember.getName())).thenReturn(confluenceAdminMember);

        /* Mock scenario where "new-group" has no members. */
        when(userAccessor.getGroup(newGroupName)).thenReturn(newGroup);
        when(userAccessor.getMemberNames(newGroup)).thenReturn(
                new DefaultPager<String>(Collections.<String> emptyList())
        );
        
        /* Mock scenario where "inactive-member" in group "inactive-group" is valid but not active. */
        when(userAccessor.getGroup(inactiveUsersOnlyGroupName)).thenReturn(inactiveUsersGroup);
        when(userAccessor.getMemberNames(inactiveUsersGroup)).thenReturn(
                new DefaultPager<String>(Arrays.asList( inactiveMember.getName() ))
        );
        when(userAccessor.isDeactivated(inactiveMember)).thenReturn(true);

        macroParameters.put("groups", ", ,,confluence-users, " + StringUtils.join(Arrays.asList(confluenceAdminGroupName, newGroupName, inactiveUsersOnlyGroupName), ", "));

        userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(), argThat(
                new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, ?> contextMap = (Map<String, ?>) o;
                        @SuppressWarnings("unchecked")
                        List<UserList> userLists = (List<UserList>) contextMap.get("userlists");

                        return new HashSet<String>(blacklistedGroups).equals(contextMap.get("deniedGroups"))
                                && StringUtils.equals("userlister.noresultsfoundforgroups", (String) contextMap.get("emptyGroups"))
                                && null != userLists
                                && 1 == userLists.size()
                                && null != userLists.get(0)
                                && StringUtils.equals(confluenceAdminGroupName, userLists.get(0).getGroup());
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not the context map expected");
                    }
                }
        ));
    }

    public void testGroupsProperlyGroupedIntoDeniedAllowedAndEmptyCategoriesWhenWilcardSpecified() throws MacroException
    {
        final List<String> blacklistedGroups = Arrays.asList( "confluence-users" );
        final String confluenceUsersGroupName = "confluence-users";
        final Group confluenceUsersGroup = new DefaultGroup(confluenceUsersGroupName);
        final String confluenceAdminGroupName = "confluence-administrators";
        final Group confluenceAdminGroup = new DefaultGroup(confluenceAdminGroupName);
        final User confluenceAdminMember = new DefaultUser("admin");
        final String newGroupName = "new-group";
        final Group newGroup = new DefaultGroup(newGroupName);
        final String inactiveUsersOnlyGroupName = "inactive-group";
        final Group inactiveUsersGroup = new DefaultGroup(inactiveUsersOnlyGroupName);
        final User inactiveMember = new DefaultUser("inactive-member");
        final List<Group> allGroups = Arrays.asList(confluenceUsersGroup, confluenceAdminGroup, inactiveUsersGroup, newGroup);
        final Pager<Group> allGroupsPager = new DefaultPager<Group>(allGroups);

        when(userListManager.getGroupBlackList()).thenReturn(new HashSet<String>(blacklistedGroups));

        /* Returns all accessible groups by the authenticated user */
        when(userAccessor.getGroups()).thenReturn(allGroupsPager);
        when(userAccessor.filterUnaccessibleGroups(same(allGroupsPager), (User) anyObject())).thenReturn(allGroupsPager);

        /* Mock scenario where "admin" in group "confluence-administrators" is valid and active. */
        when(userAccessor.getGroup(confluenceAdminGroupName)).thenReturn(confluenceAdminGroup);
        when(userAccessor.getMemberNames(confluenceAdminGroup)).thenReturn(
                new DefaultPager<String>(Arrays.asList(confluenceAdminMember.getName()))
        );
        when(userAccessor.getUser(confluenceAdminMember.getName())).thenReturn(confluenceAdminMember);

        /* Mock scenario where "new-group" has no members. */
        when(userAccessor.getGroup(newGroupName)).thenReturn(newGroup);
        when(userAccessor.getMemberNames(newGroup)).thenReturn(
                new DefaultPager<String>(Collections.<String> emptyList())
        );
        
        /* Mock scenario where "inactive-member" in group "inactive-group" is valid but not active. */
        when(userAccessor.getGroup(inactiveUsersOnlyGroupName)).thenReturn(inactiveUsersGroup);
        when(userAccessor.getMemberNames(inactiveUsersGroup)).thenReturn(
                new DefaultPager<String>(Arrays.asList( inactiveMember.getName()))
        );
        when(userAccessor.getUser(inactiveMember.getName())).thenReturn(inactiveMember);
        when(userAccessor.isDeactivated(inactiveMember)).thenReturn(true);

        macroParameters.put("groups", UserList.ALL_GROUP_NAME);

        userLister.execute(macroParameters, StringUtils.EMPTY, pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(), argThat(
                new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {

                        @SuppressWarnings("unchecked")
                        Map<String, ?> contextMap = (Map<String, ?>) o;
                        @SuppressWarnings("unchecked")
                        List<UserList> userLists = (List<UserList>) contextMap.get("userlists");

                        return new HashSet<String>(blacklistedGroups).equals(contextMap.get("deniedGroups"))
                                && StringUtils.equals("userlister.noresultsfoundforgroups", (String) contextMap.get("emptyGroups"))
                                && null != userLists
                                && 1 == userLists.size()
                                && null != userLists.get(0)
                                && StringUtils.equals(confluenceAdminGroupName, userLists.get(0).getGroup());
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not the context map expected");
                    }
                }
        ));
    }

    public void testListModeIsAllWhenOnlineParameterNotSpecifed() throws MacroException
    {
        macroParameters.put("groups", "confluence-users");
        userLister.execute(macroParameters, "", pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(), argThat(
                new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {
                        @SuppressWarnings("unchecked")
                        Map<String, ?> contextMap = (Map<String, ?>) o;

                        return !contextMap.containsKey("online")
                                && (Boolean) contextMap.get("allUserStatuses");
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not the context map expected");
                    }
                }
        ));
    }

    public void testListModeIsOnlineWhenOnlineParameterIsSetToTrue() throws MacroException
    {
        macroParameters.put("groups", "confluence-users");
        macroParameters.put("online", "true");

        userLister.execute(macroParameters, "", pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(), argThat(
                new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {

                        @SuppressWarnings("unchecked")
                        Map<String, ?> contextMap = (Map<String, ?>) o;

                        return (Boolean) contextMap.get("online");
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not the context map expected");
                    }
                }
        ));
    }

    public void testListModeIsOfflineWhenOnlineParameterIsNotSetToTrue() throws MacroException
    {
        macroParameters.put("groups", "confluence-users");
        macroParameters.put("online", "invalid");

        userLister.execute(macroParameters, "", pageToRender.toPageContext());

        verify(velocityHelperService).getRenderedTemplate(anyString(), argThat(
                new BaseMatcher<Map<String, ?>>()
                {
                    public boolean matches(Object o)
                    {

                        @SuppressWarnings("unchecked")
                        Map<String, ?> contextMap = (Map<String, ?>) o;

                        return !((Boolean) contextMap.get("online"));
                    }

                    public void describeTo(Description description)
                    {
                        description.appendText("Not the context map expected");
                    }
                }
        ));
    }

    public void testMacroGeneratesBlockHtml()
    {
        assertEquals(TokenType.BLOCK, userLister.getTokenType(null, null, null));
    }

    public void testMacroHasNoBody()
    {
        assertFalse(userLister.hasBody());
    }

    public void testMacroRendersEverythingInBody()
    {
        assertEquals(RenderMode.NO_RENDER, userLister.getBodyRenderMode());
    }

    public void testErrorMessageReturnedWhenUserDoesNotHavePermissionToViewUserProfile() throws MacroExecutionException
    {
        when(permissionManager.hasPermission(
                Matchers.<User>anyObject(),
                eq(Permission.VIEW),
                eq(PermissionManager.TARGET_PEOPLE_DIRECTORY)
        )).thenReturn(false);
        assertEquals("<div class=\"error\"><span class=\"error\">userlister.notpermitted.viewuserprofile</span> </div>", userLister.execute(new HashMap<String, String>(), null, (ConversionContext) null));
    }
}

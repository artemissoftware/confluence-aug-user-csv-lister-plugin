package com.atlassian.confluence.extra.userlister;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCache;
import com.atlassian.confluence.extra.userlister.model.UserList;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultUserListManagerTestCase extends TestCase
{

    @Mock
    private BandanaManager bandanaManager;

    @Mock
    private CacheManager cacheManager;

    private Cache cache;

    private DefaultUserListManager defaultUserListManager;

    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        cache = new MemoryCache("loggedInUsers");
        when(cacheManager.getCache(DefaultUserListManager.class.getName())).thenReturn(cache);

        defaultUserListManager = new DefaultUserListManager(bandanaManager, cacheManager);
    }

    public void testBlacklistedGroupsListReturnedNotNullWhenBandanaReturnsNull()
    {
        Set blacklistedGroups;
        
        assertNotNull(blacklistedGroups = defaultUserListManager.getGroupBlackList());
        assertEquals(0, blacklistedGroups.size());
    }

    public void testBlacklistedGroupsListReturnedAccordingToValueSavedInBandana()
    {
        final Collection<String> blackListedGroups = Arrays.asList( "confluence-users", "confluence-administrators" );

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, UserListManager.BANDANA_KEY_BLACK_LIST)).thenReturn(
                new HashSet<String>(blackListedGroups)
        );

        assertEquals(
                new HashSet<String>(blackListedGroups),
                defaultUserListManager.getGroupBlackList());
    }

    public void testBlacklistedGroupsSavedIntoBandana()
    {
        final Collection<String> blackListedGroups = Arrays.asList( "confluence-users", "confluence-administrators" );

        defaultUserListManager.saveGroupBlackList(new HashSet<String>(blackListedGroups));
        verify(bandanaManager).setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, UserListManager.BANDANA_KEY_BLACK_LIST,
                new HashSet<String>(blackListedGroups));
    }

    public void testAsteriskInBlacklistedGroupsOnlyDeniesWildcards()
    {
        final Collection<String> blackListedGroups = Arrays.asList( UserList.ALL_GROUP_NAME );

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, UserListManager.BANDANA_KEY_BLACK_LIST)).thenReturn(
                new HashSet<String>(blackListedGroups)
        );

        assertFalse(defaultUserListManager.isGroupPermitted(UserList.ALL_GROUP_NAME));
        assertTrue(defaultUserListManager.isGroupPermitted("confluence-users"));
    }

    public void testBlankGroupNamesAlwaysPermitted()
    {
        assertTrue(defaultUserListManager.isGroupPermitted("  "));
        assertTrue(defaultUserListManager.isGroupPermitted(StringUtils.EMPTY));
    }

    public void testRegisterLoggedInUser()
    {
        String sessionId = "1";
        String userName = "admin";

        defaultUserListManager.registerLoggedInUser(userName, sessionId);

        assertEquals(new HashSet<String>(Arrays.asList(sessionId)), cache.get(userName));
    }

    public void testUnregisterLoggedInUser()
    {
        String sessionId = "1";
        String userName = "admin";

        defaultUserListManager.registerLoggedInUser(userName, sessionId);
        assertEquals(new HashSet<String>(Arrays.asList(sessionId)), cache.get(userName));

        defaultUserListManager.unregisterLoggedInUser(userName, sessionId);
        assertNull(cache.get(userName));
    }

    public void testUnregisterLoggedInUserWithExistingSession()
    {
        String sessionId = "1";
        String secondSessionId = "2";
        String userName = "admin";

        defaultUserListManager.registerLoggedInUser(userName, sessionId);
        defaultUserListManager.registerLoggedInUser(userName, secondSessionId);

        assertEquals(new HashSet<String>(Arrays.asList(sessionId, secondSessionId)), cache.get(userName));

        defaultUserListManager.unregisterLoggedInUser(userName, secondSessionId);
        assertEquals(new HashSet<String>(Arrays.asList(sessionId)), cache.get(userName));
    }

    public void testGetLoggedInUserNames()
    {
        String userName = "admin";
        String userName2 = "jdoe";

        defaultUserListManager.registerLoggedInUser(userName, "1");
        defaultUserListManager.registerLoggedInUser(userName2, "2");

        assertEquals(
                new HashSet<String>(Arrays.asList(userName, userName2)),
                defaultUserListManager.getLoggedInUsers()
        );
    }
}

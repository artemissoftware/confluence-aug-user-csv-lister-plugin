package com.atlassian.confluence.extra.userlister.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.userlister.UserListManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.Group;
import com.atlassian.user.impl.DefaultGroup;
import com.opensymphony.xwork.Action;
import junit.framework.TestCase;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConfigureUserListsActionTestCase extends TestCase
{
    @Mock
    private UserAccessor userAccessor;
    
    @Mock
    private UserListManager userListManager;

    private ConfigureUserListsAction configureUserListsAction;

    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        configureUserListsAction = new ConfigureUserListsAction()
        {
            public String getText(String key)
            {
                return key;
            }

            public String getText(String key, Object[] substitutions)
            {
                return key + ':' + '[' + substitutions + ']';
            }
        };
        configureUserListsAction.setUserAccessor(userAccessor);
        configureUserListsAction.setUserListManager(userListManager);
    }

    public void testBlackListedGroupsPopulatedAfterDoDefault() throws Exception
    {
        final Set<String> blacklistedGroups = new HashSet<String>(Arrays.asList("confluence-users", "confluence-groups"));

        when(userListManager.getGroupBlackList()).thenReturn(blacklistedGroups);

        assertEquals(Action.SUCCESS, configureUserListsAction.doDefault());
        assertTrue(
                ArrayUtils.isEquals(
                        blacklistedGroups.toArray(new String[blacklistedGroups.size()]),
                        StringUtils.split(configureUserListsAction.getBlackListEntries(), "\n")
                )
        );
    }

    public void testActionResultIsCancelIfSavePropertyIsBlank() throws Exception
    {
        assertEquals(ConfluenceActionSupport.CANCEL, configureUserListsAction.execute());
    }

    public void testBlacklistedGroupsSavedIfSavePropertyIsNotBlank() throws Exception
    {
        final String[] blacklistedGroupsArray = {"confluence-users", "confluence-groups"};
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");
        final Collection actionMessages;

        configureUserListsAction.setSave("add.name");
        configureUserListsAction.setBlackListEntries(blacklistedEntries);

        assertEquals(ConfluenceActionSupport.SUCCESS, configureUserListsAction.execute());

        verify(userListManager).saveGroupBlackList(
                new HashSet<String>(Arrays.asList(blacklistedGroupsArray))
        );

        actionMessages = configureUserListsAction.getActionMessages();
        assertNotNull(actionMessages);
        assertEquals(1, actionMessages.size());
        assertEquals("userlister.configure.successful", actionMessages.iterator().next());
    }

    public void testValidationNotPerformedIfCancelPropertyIsNotBlank() throws Exception
    {
        configureUserListsAction.setCancel("back.name");
        configureUserListsAction.validate();
        
        assertFalse(configureUserListsAction.hasErrors());
    }

    public void testValidationPassesWhenAsteriskIsSpecifeidAsOneBlacklistEntry() throws Exception
    {
        final String asterisk = "*";
        final String[] blacklistedGroupsArray = { asterisk };
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");
        

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        verify(userAccessor, never()).getGroup(asterisk);
        
        assertFalse(configureUserListsAction.hasErrors());
    }

    public void testValidationFailsWhenAnInvalidGroupIsSpecifiedAsOneBlacklistEntry() throws Exception
    {
        final String invalidGroupName = "invalid-group";
        final String[] blacklistedGroupsArray = { invalidGroupName };
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        assertTrue(configureUserListsAction.hasErrors());
        final Iterable<String> actionErrors = configureUserListsAction.getActionErrors();

        assertNotNull(actionErrors);
        assertTrue(getOnlyElement(actionErrors).startsWith("userlister.configure.invalid.group.names"));
    }

    public void testValidationFailsWhenAtLeastOneBlacklistEntryIsInvalid() throws Exception
    {
        final String invalidGroupName = "invalid-group";
        final String invalidGroupName2 = "invalid-group2";
        final String validGroupName = "confluence-users";
        final String validGroupName2 = "confluence-administrators";
        final String[] blacklistedGroupsArray = { invalidGroupName, invalidGroupName2, validGroupName, validGroupName2 };
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");

        when(userAccessor.getGroup(anyString())).thenAnswer(
                new Answer<Group>()
                {
                    public Group answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        String arg = (String) invocationOnMock.getArguments()[0].toString();
                        if (arg.equals(validGroupName) || arg.equals(validGroupName2))
                        {
                            return new DefaultGroup(arg);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
        );

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        assertTrue(configureUserListsAction.hasErrors());

        final Iterable<String> actionErrors = configureUserListsAction.getActionErrors();

        assertNotNull(actionErrors);
        assertTrue(getOnlyElement(actionErrors).startsWith("userlister.configure.invalid.group.names"));
    }

    public void testErrorMessageIsEncoded() throws Exception
    {
        final String invalidGroupName = "<script>alert(document.cookie);</script>";
        final String[] blacklistedGroupsArray = { invalidGroupName };
        final String blacklistedEntries = StringUtils.join(blacklistedGroupsArray, "\n");

        configureUserListsAction.setBlackListEntries(blacklistedEntries);
        configureUserListsAction.validate();

        assertTrue(configureUserListsAction.hasErrors());
        final Iterable<String> actionErrors = configureUserListsAction.getActionErrors();

        assertNotNull(actionErrors);
        assertTrue(getOnlyElement(actionErrors).startsWith("userlister.configure.invalid.group.names"));
        assertFalse(getOnlyElement(actionErrors).contains(invalidGroupName));
    }

}

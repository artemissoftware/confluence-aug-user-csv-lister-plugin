package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.event.Event;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import junit.framework.TestCase;
import org.apache.commons.lang.RandomStringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Mockito.verify;

public class UserListenerTestCase extends TestCase
{
    private UserListener userListener;

    @Mock
    private UserListManager userListManager;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);


        userListener = new UserListener(userListManager);
    }

    protected void tearDown() throws Exception
    {
        ContainerManager.getInstance().setContainerContext(null);
        super.tearDown();
    }

    public void testHandledClasses()
    {
        assertEquals(
                Arrays.asList(LoginEvent.class, LogoutEvent.class),
                Arrays.asList(userListener.getHandledEventClasses()));
    }

    public void testHandleUserLoginEvent()
    {
        final User userWhoLoggedIn = new DefaultUser("admin");
        final String _sessionId = RandomStringUtils.randomAlphanumeric(32);
        final LoginEvent loginEvent = new LoginEvent(this, userWhoLoggedIn.getName(), _sessionId, "localhost", "127.0.0.1", null);

        userListener.handleEvent(loginEvent);
        verify(userListManager).registerLoggedInUser(userWhoLoggedIn.getName(), _sessionId);
    }

    public void testHandleUserLoggedOutEvent()
    {
        final User userWhoLoggedOut = new DefaultUser("admin");
        final String _sessionId = RandomStringUtils.randomAlphanumeric(32);
        final LogoutEvent logoutEvent = new LogoutEvent(this, userWhoLoggedOut.getName(), _sessionId, "localhost", "127.0.0.1");
        
        userListener.handleEvent(logoutEvent);
        verify(userListManager).unregisterLoggedInUser(userWhoLoggedOut.getName(), _sessionId);
    }

    public void testUserListerMacroNotNotifiedOfLoginOrLogoutEventsWhenListenerHandlesEventsNotInItsHandlingList()
    {
        userListener = new UserListener(userListManager)
        {
            public UserLister getUserLister()
            {
                return new UserLister(null, null, null, null, null, null, null)
                {
                    public void userLoggedIn(String username, String sessionId)
                    {
                        fail("This method should not be called when handling events outside of the listener's scope.");
                    }

                    public void userLoggedOut(String sessionId)
                    {
                        fail("This method should not be called when handling events outside of the listener's scope.");
                    }
                };
            }
        };

        userListener.handleEvent(new Event(this));
    }
}
